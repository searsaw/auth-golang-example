package http

import (
	"context"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"time"

	authgolangexample "gitlab.com/searsaw/auth-golang-example"
	"gitlab.com/searsaw/auth-golang-example/pkg/controller"
)

type Transport struct {
	server     http.Server
	controller *controller.AuthController
	templates  *template.Template
	port       string
}

func NewTransport(cntlr *controller.AuthController, port string) *Transport {
	return &Transport{controller: cntlr, port: port}
}

func (t Transport) Run(ctx context.Context) error {
	if err := t.parseTemplates(); err != nil {
		log.Fatalf("error parsing the templates: %s\n", err)
	}

	router := http.NewServeMux()
	router.Handle("/assets", http.FileServer(http.Dir("public")))
	router.HandleFunc("/register", t.handlerRegister)
	router.HandleFunc("/login", t.handlerLogin)
	router.HandleFunc("/dashboard", t.handleDashboard)
	router.HandleFunc("/", t.handleRoot)

	// create server
	t.server = http.Server{Addr: ":" + t.port, Handler: router}

	var err error
	wait := make(chan struct{})
	go func() {
		<-ctx.Done()
		c, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		if err := t.server.Shutdown(c); err != nil {
			err = fmt.Errorf("error closing the server down: %w", err)
		}
		close(wait)
	}()

	fmt.Printf("server running on :%s\n", t.port)
	if err := t.server.ListenAndServe(); err != http.ErrServerClosed {
		return fmt.Errorf("error starting the http server: %w", err)
	}

	<-wait

	return err
}

func (t *Transport) parseTemplates() error {
	cwd, err := os.Getwd()
	if err != nil {
		return fmt.Errorf("error getting the cwd: %s", err)
	}
	t.templates = template.Must(template.ParseGlob(filepath.Join(cwd, "templates", "*.gohtml")))

	return nil
}

func (t *Transport) displayTemplate(w http.ResponseWriter, r *http.Request, templateName string) {
	w.WriteHeader(http.StatusOK)
	if err := t.templates.ExecuteTemplate(w, templateName+".gohtml", nil); err != nil {
		log.Printf("error writing the index.gohtml template: %s", err)
	}
}

func (t *Transport) handleRoot(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		t.displayTemplate(w, r, "index")
	}
}

func (t *Transport) handleDashboard(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		t.displayTemplate(w, r, "dashboard")
	default:
		t.handleError(w)
	}
}

func (t *Transport) handlerRegister(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		t.displayTemplate(w, r, "register")
	case http.MethodPost:
		if err := r.ParseForm(); err != nil {
			log.Printf("error parsing the form: %s", err)
			t.handleError(w)
			return
		}

		firstName := r.Form.Get("firstName")
		lastName := r.Form.Get("lastName")
		username := r.Form.Get("username")
		password := r.Form.Get("password")
		user := &authgolangexample.User{
			FirstName: firstName,
			LastName:  lastName,
			Username:  username,
			Password:  password,
		}
		if _, err := t.controller.HandleRegister(r.Context(), user); err != nil {
			log.Printf("error registering the user: %s", err)
			t.handleError(w)
			return
		}
		http.Redirect(w, r, "/dashboard", http.StatusFound)
	default:
		t.handleError(w)
	}
}

func (t *Transport) handlerLogin(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		if err := r.ParseForm(); err != nil {
			log.Printf("error parsing the form: %s", err)
			t.handleError(w)
			return
		}
		username := r.Form.Get("username")
		password := r.Form.Get("password")
		_, err := t.controller.HandleLogin(r.Context(), username, password)
		if err == controller.ErrIncorrectPassword {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		if err != nil {
			log.Printf("error verifying the user: %s", err)
			t.handleError(w)
			return
		}
		http.Redirect(w, r, "/dashboard", http.StatusFound)
	default:
		t.handleError(w)
	}
}

func (t *Transport) handleError(w http.ResponseWriter) {
	w.WriteHeader(http.StatusBadRequest)
}

func (t Transport) Stop(ctx context.Context) error {
	panic("implement me")
}
