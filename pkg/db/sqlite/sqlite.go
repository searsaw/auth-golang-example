package sqlite

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	_ "github.com/mattn/go-sqlite3"

	authgolangexample "gitlab.com/searsaw/auth-golang-example"
)

type Sqlite struct {
	db *sql.DB
}

func NewSqlite(filename string) (*Sqlite, error) {
	db, err := sql.Open("sqlite3", filename)
	if err != nil {
		return nil, fmt.Errorf("error opening the sqlite3 database: %w", err)
	}

	createQuery := `CREATE TABLE IF NOT EXISTS users (
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		first_name TEXT NOT NULL,
		last_name TEXT NOT NULL,
		username TEXT NOT NULL,
		password TEXT NOT NULL,
		created_at DATETIME NOT NULL DEFAULT current_timestamp,
		updated_at DATETIME NOT NULL DEFAULT current_timestamp
	);`
	if _, err := db.Exec(createQuery); err != nil {
		return nil, fmt.Errorf("error creating the default schema: %w", err)
	}

	return &Sqlite{db: db}, nil
}

func (s *Sqlite) CreateUser(ctx context.Context, user *authgolangexample.User) (*authgolangexample.User, error) {
	hashed, err := user.HashedPassword()
	if err != nil {
		return nil, fmt.Errorf("error generating the password: %s", err)
	}
	user.Password = hashed

	tx, err := s.db.Begin()
	if err != nil {
		return nil, fmt.Errorf("error creating the user: %w", err)
	}

	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()
	result, err := tx.ExecContext(
		ctx, "INSERT INTO users (first_name, last_name, username, password) VALUES (?, ?, ?, ?);",
		user.FirstName, user.LastName, user.Username, user.Password)
	if err != nil {
		return nil, fmt.Errorf("error inserting the user: %w", err)
	}

	if err := tx.Commit(); err != nil {
		return nil, fmt.Errorf("error committing the transaction: %w", err)
	}

	id, err := result.LastInsertId()
	if err != nil {
		return nil, fmt.Errorf("error getting the last ID: %w", err)
	}

	user.ID = id

	return user, nil
}

func (s *Sqlite) GetUserByUsername(ctx context.Context, username string) (*authgolangexample.User, error) {
	user := &authgolangexample.User{}

	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()
	row := s.db.QueryRowContext(
		ctx, "SELECT id, first_name, last_name, username, password FROM users WHERE username = ?;", username)
	if err := row.Scan(&user.ID, &user.FirstName, &user.LastName, &user.Username, &user.Password); err != nil {
		return nil, fmt.Errorf("error scanning the row in: %w", err)
	}

	return user, nil
}

func (s *Sqlite) Close() error {
	if err := s.db.Close(); err != nil {
		return fmt.Errorf("error closing the sqlite3 connection: %w", err)
	}
	return nil
}
