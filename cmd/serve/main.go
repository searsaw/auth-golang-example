package main

import (
	"context"
	"flag"
	"fmt"
	"html/template"
	"log"
	"os"
	"os/signal"
	"syscall"

	"golang.org/x/sync/errgroup"

	"gitlab.com/searsaw/auth-golang-example/pkg/controller"
	"gitlab.com/searsaw/auth-golang-example/pkg/db/sqlite"
	"gitlab.com/searsaw/auth-golang-example/pkg/transport"
	"gitlab.com/searsaw/auth-golang-example/pkg/transport/http"
)

var (
	templates *template.Template
	auth      *controller.AuthController
)

func main() {
	// create flags to customize server
	port := flag.String("port", "8080", "the port to run the server on")
	dbFile := flag.String("db", "auth_golang.db", "the SQLite database file")
	flag.Parse()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	dal, err := sqlite.NewSqlite(*dbFile)
	if err != nil {
		log.Fatalf("error opening the database: %s\n", err)
	}
	defer dal.Close()

	auth = controller.NewAuthController(dal)

	transports := []transport.Transport{http.NewTransport(auth, *port)}

	group, groupCtx := errgroup.WithContext(ctx)
	for _, t := range transports {
		group.Go(func() error {
			return t.Run(ctx)
		})
	}

	// create channel to listen for kernel signals
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		// wait on a signal
		<-signals
		cancel()
	}()

	go func() {
		// wait on an error from the errgroup
		<-groupCtx.Done()
		cancel()
	}()

	// wait for shutdown to finish
	if err := group.Wait(); err != nil {
		fmt.Printf("error when waiting on the group: %s", err)
		os.Exit(1)
	}
	fmt.Println("servers shutdown successfully")
}
