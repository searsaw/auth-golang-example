module gitlab.com/searsaw/auth-golang-example

go 1.13

require (
	github.com/mattn/go-sqlite3 v2.0.2+incompatible
	golang.org/x/crypto v0.0.0-20200109152110-61a87790db17
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e
)
